#include <iostream>
#include <algorithm>
#include <vector>
#include <list>

typedef int int32;
typedef unsigned int uint32;

typedef long long int64;
typedef unsigned long long uint64;


struct Edge {
    uint32 u, v;
    int64 capacity, flow;

    Edge(uint u, uint v, int64 capacity, int64 flow = 0)
    : u(u), v(v), capacity(capacity), flow(flow) {}
};

struct Graph {
    std::vector< Edge > edges;
    std::vector< std::vector< uint32 > > adjacent;
    std::vector< uint32 > current;

    std::vector< int64 > excess;
    std::vector< uint32 > height;

    std::list< uint32 > vertexes;

    Graph(uint32 _size) {
        adjacent.resize(_size + 1);
        current.resize(_size + 1, 0);
        excess.resize(_size + 1, 0);
        height.resize(_size + 1, 0);
        height[1] = _size;
        for (uint32 i = 2; i < _size; ++i)
            vertexes.push_back(i);
    }

    void addEdge(uint32 _u, uint32 _v, int64 _capacity) {
        int64 flow = (_u == 1 ? _capacity : 0);
        adjacent[_u].push_back(edges.size());
        edges.push_back(Edge(_u, _v, _capacity, flow));
        adjacent[_v].push_back(edges.size());
        edges.push_back(Edge(_v, _u, 0, -flow));

        if (_u == 1)
            excess[_v] += _capacity;
    }

    Edge &getCurrentEdge(uint32 _u) {
        return edges[adjacent[_u][current[_u]]];
    }

    Edge &getOppositeCurrentEdge(uint32 _u) {
       return edges[adjacent[_u][current[_u]] ^ 1];
    }

    bool push(uint32 _u) {
        // std::cerr << "PUSH " << _u << "\n";
        Edge &edge = getCurrentEdge(_u), &oppositeEdge = getOppositeCurrentEdge(_u);
        // std::cerr << "    to " << edge.v << "\n";
        // std::cerr << "u, v height: " << height[edge.u] << " " << height[edge.v] << "\n";
        if (edge.capacity - edge.flow <= 0 || height[edge.u] <= height[edge.v]) {
            // std::cerr << "PUSH " << _u << " DONE, false\n";
            return false;
        }
        int64 delta = std::min(excess[_u], edge.capacity - edge.flow);

        excess[edge.u] -= delta;
        excess[edge.v] += delta;
        edge.flow += delta;
        oppositeEdge.flow -= delta;
        // std::cerr << "PUSH " << _u << " DONE, true\n";
        return true;
    }

    void relabel(uint32 _u) {
        // std::cerr << "RELABEL " << _u << "\n";
        current[_u] = 0;

        uint32 minHeight = -1;
        std::vector< uint32 >::iterator edgeNumberIterator;
        for (edgeNumberIterator = adjacent[_u].begin(); edgeNumberIterator != adjacent[_u].end(); ++edgeNumberIterator)
            if (edges[*edgeNumberIterator].capacity - edges[*edgeNumberIterator].flow > 0)
                minHeight = std::min(minHeight, height[edges[*edgeNumberIterator].v]);
        minHeight = minHeight == -1 ? height[_u] : minHeight;
        height[_u] = minHeight + 1;
        // std::cerr << "RELABEL " << _u << " DONE\n";
    }

    void discharge(uint32 _u) {
        // std::cerr << "DISCHARGE " << _u << "\n";
        while (excess[_u] > 0) {
            if (current[_u] == adjacent[_u].size()) {
                relabel(_u);
            }
            else if (push(_u)) {
                ;
            }
            else {
                ++current[_u];
            }
        }
        // std::cerr << "DISCHARGE " << _u << " DONE\n";
    }

    void findFlow() {
        std::list< uint32 >::iterator vertex;
        uint32 oldHeight, vertexNumber;
        for (vertex = vertexes.begin(); vertex != vertexes.end(); vertex++) {
            oldHeight = height[*vertex];
            discharge(*vertex);
            if (oldHeight != height[*vertex]) {
                vertexNumber = *vertex;
                vertexes.erase(vertex);
                vertexes.push_front(vertexNumber);
                vertex = vertexes.begin();
            }
        }
    }

    int64 getFlow() {
        int64 flow = 0;
        std::vector< uint32 >::iterator edgeNumberIterator;
        for (edgeNumberIterator = adjacent[1].begin(); edgeNumberIterator != adjacent[1].end(); ++edgeNumberIterator)
            flow += edges[*edgeNumberIterator].flow;
        return flow;
    }
};

int main() {
    // freopen("input.txt", "r", stdin);

    uint32 n, m;
    std::cin >> n >> m;

    Graph g(n);

    uint32 i, u, v;
    int64 c;

    for (i = 0; i < m; ++i) {
        std::cin >> u >> v >> c;
        g.addEdge(u, v, c);
    }

    g.findFlow();
    std::cout << g.getFlow() << "\n";
    std::vector< Edge >::iterator edgeIterator;
    for (edgeIterator = g.edges.begin(); edgeIterator != g.edges.end(); edgeIterator += 2)
        std::cout << edgeIterator->flow << "\n";
    return 0;
}